# GEO IP


### The Service
Queries GEO location information about a valid IP version 4 address.

It requires an account and API key from [ipgeolocation](https://app.ipgeolocation.io/).
You should export the API key as an environment variable called `GEOIP_IP_GEOLOCATION_API_TOKEN`.

This code assumes that you are using an Unix system, it won't work on Windows.

Other parameters can be configured, such as the ipgeolocation API base, there is a variable
variable called `GEOIP_IP_GEOLOCATION_URL`.


### Command line interface

You can use the project over a command line interface or as library. First of all, create
a virtual environment and source it. (the code assumes you are using a Python version bigger
or equals than 3.7).

* Create the virtual environment
```
python -m venv /tmp/geoip-env
source /tmp/geoip-env
```
(You can use whatever you want to create the virtual environment, I left just an example.)


* Install the project.
```
pip install .
```

To use the CLI, just type `geoip-cli --help`, it's self documented.
