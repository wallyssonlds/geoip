import click
from click import echo
import sys
from pprint import pformat

import geoip
from geoip import services


@click.group()
def cli():
    ...


@cli.command()
@click.argument('filename', type=click.Path(exists=True))
def parse(filename):
    with open(filename) as f:
        parser = geoip.Parser(f)
        for ip in parser.parse():
            echo(ip.group())


@cli.command()
@click.argument("ip")
@click.option('-s', '--service-name', default='ipgeolocation')
@click.option('-n', '--no-cache', is_flag=True, default=False)
def query(ip, service_name, no_cache):
    if service_name not in services.SERVICES:
        echo("Service not supported", err=True)
        echo("Supported services are %s" % ', '.join(services.SERVICES.keys()), err=True)
        sys.exit(1)

    service = services.SERVICES[service_name]()
    try:
        result = geoip.GeoIP(ip, service, cache=not no_cache).query()
    except AssertionError as e:
        echo(str(e), err=True)
        exit(1)

    echo(pformat(result))
