import json
import re
from itertools import chain
from os import environ



VALID_IP_REGEX = r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'


class Parser:
    def __init__(self, file):
        self.file = file

    def parse(self):
        ips = []
        for line in self.file:
            ips = chain(ips, re.finditer(VALID_IP_REGEX, line))
        return ips


class GeoIPResolver(object):
    cache_file_default_location = '/tmp/geoip_cache'
    def __init__(self, ip, service_instance, cache=True):
        self.ip = ip
        self.service_instance = service_instance
        self.cache_location = environ.get('GEOIP_CACHE_FILE_LOCATION', self.cache_file_default_location)
        self.cache = cache

    def _get_cached_results(self):
        try:
            with open(self.cache_location) as f:
                return json.load(f)
        except (IOError, ValueError):
            return {}

    def _update_cache(self, cached_results):
        with open(self.cache_location, 'w') as f:
            f.write(json.dumps(cached_results))

    def __enter__(self):
        if not self.cache:
            return self.service_instance.query(self.ip)

        cached_results = self._get_cached_results()
        if self.ip not in cached_results:
            cached_results[self.ip] = self.service_instance.query(self.ip)
            self._update_cache(cached_results)

        return cached_results[self.ip]

    def __exit__(self, *args, **kwds):
        pass


class GeoIP:
    def __init__(self, ip, service_instance, cache=True):
        assert type(ip) is str, "ip argument must be a string"
        assert re.match(VALID_IP_REGEX, ip), "the value %s is not a valid ip address" % ip

        self.cache = cache
        self.ip = ip
        self.service_instance = service_instance

    def query(self):
        with GeoIPResolver(self.ip, self.service_instance, self.cache) as result:
            return result
