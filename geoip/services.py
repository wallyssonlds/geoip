import requests
from os import environ
from requests.models import PreparedRequest


_IP_GEOLOCATION_DEFAULT_URL = "https://api.ipgeolocation.io/ipgeo"


class BaseService:
    def query(self, ip):
        raise NotImplemented



class IPGeolocationService(BaseService):
    def query(self, ip):
        base_url = environ.get("GEOIP_IP_GEOLOCATION_URL", _IP_GEOLOCATION_DEFAULT_URL)
        api_key = environ.get("GEOIP_IP_GEOLOCATION_API_TOKEN")
        assert api_key, "you must export GEOIP_IP_GEOLOCATION_API_TOKEN as environ variable"
        url_params = {"apiKey": api_key, "ip": ip}
        # TODO: handle http errros
        data = requests.get(base_url, params=url_params).json()
        return dict(
            city=data['city'],
            country=data['country_name'],
            ip=data['ip'],
            current_time=data['time_zone']['current_time'],
            latitude=data['latitude'],
            longitude=data['longitude']
        )


SERVICES = {
    'ipgeolocation': IPGeolocationService,
}
